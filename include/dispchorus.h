/*   CHORUS-specific dispatcher routines */
/*
 $Id: dispchorus.h,v 1.5 1995/05/30 07:53:21 ruten Exp $
*/
#ifdef __cplusplus
extern "C" {
#endif

/* convert subsystem name to evb/dispatch host (and the tag in case of evb) */
int getevbhost(const char *subsystem, char *host, char *tag);
int getdisphost(const char *subsystem, char *host);
#ifndef OS9
int writedisphost(const char *subsystem);
int dropdisphost(const char *subsystem);
int sendevb(const char *host, const char *tag, const char *line);
int sendevb_no_wait(const char *host, const char *tag, const char *line, int delay);
#endif

#ifdef __cplusplus
  }
#endif
/*   CHORUS-specific end   */ 
