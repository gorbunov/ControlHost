/* This is a template for buliding data sending   
   client, which sends the data to the dispatcher.
*/

#include "dispatch.h"
#include <stdlib.h>
#include <stdio.h>

#define BUF_LEN 2
#define MAX_EVS 1000000
static char buf[BUF_LEN];   /* send data buffer */

int main(int argc, char *argv[])
  {
  char * host;
  int rc,i;
  int nbytes = -1;
  int send_pos = -1;
  unsigned long ev=0;

  host = "pcitep04.cern.ch"; 

  /*host = (char *)0;  if you run on the same host as dispatcher */

  rc = init_disp_link(host,"");
  if( rc < 0 )
    exit(1); /* no connection */


  for(;;)
    {
    /* main loop */ 
      ev++;
      if(ev%10000==0) printf(" event=%ld\n",ev);
      if(ev==MAX_EVS) break;

    if( nbytes < 0 )
      {
      /* the buffer is ready for new data portion */
	
	nbytes = BUF_LEN;
	for (i=0; i<nbytes; i++) buf[i]=i%256;
      send_pos = 0;
      }

    if( nbytes >= 0 )
      {
      /* the data portion sending is not finished yet 
      rc = put_data("TAG1", // 8 chars max 
              buf,nbytes,&send_pos); */
    
      rc = put_fulldata("TG",buf,nbytes);     
      /* put_data sends what can be sent and returns rc=0    
         if the data transfer is not finished completely.
         put_fulldata always send the whole block, waiting
         for the end of transmission
      */

      if( rc < 0 )
        exit(5); /* connection is broken */
      if( rc > 0 )
        {
        /* data portion shipped completely */
        send_pos = -1;
        nbytes = -1;
        }
      }

  /*do something useful
    .....................*/

    } /* end of main loop */
  exit(0);
  }
 
 
