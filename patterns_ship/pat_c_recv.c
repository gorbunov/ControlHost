


/* This is a template for buliding data processing
   client, which receives the data and commands from the dispatcher.
*/

#include "dispatch.h"
#include <stdlib.h>
#include <stdio.h>

#define LIM 2  /* set the proper value here */
//#define TAGSIZE 10

static char buf[LIM];

int main(int argc, char *argv[])
  {
  char * host;
  char tag[TAGSIZE+1];
  int rc;
  int nbytes;
  unsigned long ev=0;

  //host = "name of the host where dispatcher runs"; 

  host = (char *)0; /* if you run on the same host as dispatcher */

  rc = init_2disp_link(host,"w SOMETAG1 w SOMETAG2","a TG");

  /* we want to receive the data with the tags SOMETAG1, SOMETAG2,
     and commands with tags  SOMETAG3 ...
     If you are a local client, (i.e. you run on the same 
     host as dispatcher), you may replace
     some "w"s with "m"s. In this case the data buffers
     with corresponding tags will be shipped to you
     via shared memory, which is much faster for big
     amounts of data. Don't use "m" for small messages.
  */

  if( rc < 0 )
    exit(1); /* no connection */

#define ALWAYS 1 /* if you are capable to process all the data */
  //or
  //#define ALWAYS 0 /* if you need only a fraction */

  if( ALWAYS )
    {
    rc = send_me_always();
    if( rc < 0 )
      exit(2); /* connection is broken */
    }

  for(;;)
    {
    /* main loop */
    if( !ALWAYS )
      {
      rc = send_me_next();
      if( rc < 0 )
        exit(3); /* connection is broken */
      }
 
    rc = wait_head(tag,&nbytes); /* wait for arriving data */
    // or
    //rc = check_head(tag,&nbytes); /* just check without waiting */

    if( rc < 0 )
      exit(4); /* connection is broken */
    else if( rc > 0 )
      { 
      /* we got the tag and size (in bytes) of incoming data
         into variables "tag" and "nbytes"
      */ 
      rc = get_data(buf,LIM);
      /* get the data. If nbytes > LIM then
             the excessive data portion is simply lost.
         if nbytes <= LIM we get exactly nbytes bytes
      */
      if( rc < 0 )
        exit(5); /* connection is broken */

      /* here we have:    
         data tag in variable "tag"
         data size in variable "nbytes" 
             it reflects the amount of data sent,
             recieved portion may be less (if nbytes > sizeof(buf))
         data in array "buf"
      */

      //process the data, depending on tag
      //..........................
      ev++;
      if(ev%10000==0) printf(" recv %ld\n",ev);
      continue; /* go look for next data buffer */
      } /* end of current portion processing */

    //do something private
    //....................
    } /* end of main loop */
  }
