/** rawsaver.c
 *
 * raw data saving task for ATLAS FCal beam tests
 * Author: P.Gorbounov 2003
 ========================================================*/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include "dispatch.h"


#define ALWAYS           1
#define COPYDATA         1
#define MAX_WARNS        5
#define CRAP             10000   /* max allowed run_number */
#define SZ               27000
#define LIM              SZ*sizeof(int)
#define CMD_LEN          20

#define EV_HDR_NEW       0x00CAFE00
#define EV_HDR           14
#define EV_HDRLEN        EV_HDR*sizeof(int)
#define IOBUF_SIZE       20000

#define ConnectionFailed fini("link/read failure from dispatcher",dispatcher)

#define CloseRun \
	warns=0;\
	if( f >= 0 ) {\
	  close(f);\
	  fprintf(stderr,\
		  "File %s closed, %d events saved in %d bytes\n",\
		  filename,kev,bytes_sent);\
	  kev=bytes_sent=0;\
	}\
        prev_run_num=-1;\
        f=-1;

#define RSOF 1001
        
static int evbuf[SZ]; 
static char iobuf[IOBUF_SIZE];

static int prev_run_num = -1, ev_onl, kev,bytes_sent;

static char* timestamp(void);
void fini(char* stop_message, char* aux_message);

void main(int argc, char *argv[])
{
  char *tag="EVTSPEC", 
       *dispatcher="", 
       *default_path="/dev/null",
       subscrdata[50];

  char in_tag[TAGSIZE+1],
       command[CMD_LEN];

  FILE* input_file=NULL;

  int  *evadr=evbuf+EV_HDR;   /* reserve EV_HDR words for a "new" ev header */

  int copydata;
  int local;
  int debug=0;
  int lim = SZ;
  int f = -1;
  int i,rc;
  int addr;
  int sz;
  int key;
  int warns=0;
  int bk_head_len;
  int run_num,run_type,ev_onl,ev_label;
  char filename[500];
  long get_data_cnt = 0;

/* interpret the command line arguments */

  if( argc >1 ) {
    if(strcmp(argv[1],"-?") == 0) {
      printf("\n rawsaver [event TAG to save, <=8 letters (default=EVTSPEC)]\n"
             "          [dispatcher-address or LOCAL (default)]\n"
             "          [save-file path (default=%s)]\n"
	     "          [debug level (default=0)]\n\n",default_path);
      exit(0);
    }

    tag=argv[1];

    strcpy(subscrdata,"am ");
    strcat(subscrdata,tag);
    if( argc >= 3 && strspn(argv[2],"LOCALlocal")!=5) {
      dispatcher=argv[2];
      strcat(subscrdata," a ENDRUN");
      local=0;
    }
    else {
      local = 1;
    }

    if( argc >= 4 ) default_path=argv[3];
    if( argc >= 5 ) sscanf(argv[4],"%i",&debug);
    

  }


/* try to connect to the dispatcher */
/*		     strcat(subscrdata,tag), */

  if(init_2disp_link(dispatcher,subscrdata,"a CMDSPEC" ) < 0)  
    fini("Failure to connect to the dispatcher ",dispatcher);

  printf("\n *** rawsaver:\n"
	 "     Saving data with from the dispatcher %s\n"
	 "     subscription for data: %s\n"
	 "     subscription for commands: a CMDSPEC\n"
         "     save-data path: %s\n"
	 "     debug level: %i\n\n",
	 dispatcher,subscrdata,default_path,debug);


/***********************************************************************
  main event loop
***********************************************************************/


  copydata = (!local || COPYDATA);
/*  evbuf[1]=EV_HDR_NEW; */
  bytes_sent=kev=0;

  evbuf[5]=0;
  evbuf[8] =0xCAFE001;
  evbuf[9] =0xCAFE002;
  evbuf[10]=0xCAFE003;


#ifdef ALWAYS
  send_me_always();
#endif

  for(;;) {

#ifndef ALWAYS
    if(send_me_next()<0)                           ConnectionFailed;
#endif

    if(wait_head(in_tag,&sz)<0)                    ConnectionFailed;

/*    printf("Got TAG:%s\n",in_tag); */

/* command */

    if( strcmp(in_tag,"CMDSPEC")==0) {
      if(get_data(command,CMD_LEN)<0)              ConnectionFailed;
      if(debug) printf("Got command:%s\n",command);
      if (strcmp(command,"StopRawsaver")==0) 
	fini("StopRawsaver command received"," ");
      if (strcmp(command,"TellRawsaver")==0) {
	printf("\n *** rawsaver: receiving run %d, ev. %d, my ev. %d, ",
	       run_num, ev_onl, kev);
	if(warns) printf("not saving (funny run_number)");
	else printf("%d bytes saved\n\n",bytes_sent);
      }
      if (strcmp(command,"AcknRawsaver")==0) {
/*	printf("Ackn\n"); */
	put_fulldata("RAWSAVER","is there",8);
      }
      if( strcmp(command,"EndRun")==0) {
	printf("\n*rawsaver* got EndRun after %d events (%d bytes)\n\n",
	       kev,bytes_sent);
	CloseRun;
      }
    }

    else {

/* event */

      if(copydata) {
        evadr=evbuf+EV_HDR;   /* reserve EV_HDR words for a "new" ev header */
	rc=get_data((char *)evadr,LIM-EV_HDRLEN);
fprintf(stderr,"get_data %ld\n",get_data_cnt++);
      }
      else {
	evadr = (int *)get_data_addr();
	rc=(evadr==NULL)?  -1:0;
	if(rc>=0) memcpy((char *)(evbuf+EV_HDR),(char *)evadr,sz);
	unlock_data();
fprintf(stderr,"get_data_addr %ld\n",get_data_cnt++);
      }
      if(debug>1) printf("Got event sz=%d bytes evadr=%p rc=%d\n",sz,evadr,rc);
      if( rc < 0 )                                 ConnectionFailed;
      addr=0;
      do{
	if(*(evadr+2+addr) == RSOF) {
	  bk_head_len=*(evadr+1+addr);
	  run_num  = *(evadr + bk_head_len+addr); 
	  run_type = *(evadr + bk_head_len+1+addr);
	  ev_onl   = *(evadr + 4+addr);
	  goto got_RSOF;
	}
	else {
	  addr+=*(evadr); if(addr>=sz/sizeof(int)) break;
	}
      } while (addr<sz/sizeof(int));

      printf("No RSOF found\n");
      continue; /* no RSOF found... Strange! */

    got_RSOF:

      if(debug) {
	if(debug>1) printf("Found RSOF: run %d ev %d\n",run_num, ev_onl); 
	addr=0;
	do{
	  key=*(evadr+2+addr);
	  if(debug>1 || (key>1090 && key<1100) )
	  printf("     Bank: len %d hdr_len %d bk id %d\n", 
		 *(evadr+addr),*(evadr+1+addr),key);
	  addr+=*(evadr+addr); if(addr>=sz/sizeof(int)) break;
	} while (addr<sz/sizeof(int));
      }

      if( run_num != prev_run_num ) {
	if(run_num<0 || run_num>CRAP) {
	  if(++warns<=MAX_WARNS) 
	    printf("\n\a *** rawsaver: funny run_number received: %d\n"
		   "     May be wrong event format. Ignored.\n",run_num);
	  continue;
	}

        CloseRun;

	prev_run_num = run_num;
	if (strcmp(default_path,"/dev/null")==0) 
	  strcpy(filename,"/dev/null");
	else
	  sprintf(filename,"%s%d%c",default_path,run_num,0);

/******* open output file, if it exists, create an emergency name and open ***/

	  f = open(filename, O_WRONLY | O_CREAT | O_EXCL, 0444);
	  if( f < 0 ) {
	    perror("cannot create file, will use timestamp");
	    sprintf(filename,"%s%d.%s%c",default_path,run_num,timestamp(),0);
	    f = open(filename, O_WRONLY | O_CREAT | O_EXCL, 0444);
	    if( f < 0 ) {
	      perror("\nSomething is wrong: cannot overcome file open"
		     "failure. Stop\a\n");
	      exit(2000);
	    }
	  }
	  
/****************************************************************************/
	
/*	rc=setvbuf(f,iobuf,_IOFBF,IOBUF_SIZE); 
		filename,LIM,rc); */
	printf("New file %s opened\n",filename);
      }
      
      
/* build the event header out of RSOF bank */

      evbuf[0]=(sz+EV_HDRLEN)/sizeof(int);
      evbuf[1]=EV_HDR;
      evbuf[2]=evbuf[11]=run_type;
      evbuf[3]=*(evadr+3);
      evbuf[4]=ev_onl;

      evbuf[6]=*(evadr+6); /* date */
      evbuf[7]=*(evadr+7); /* time */
      evbuf[12]=-ev_onl;
      evbuf[13]=-run_num;
/*
printf("Next : "); for(i=0;i<16;i++) printf("%8x ",evbuf[i]);
printf("\n     : "); for(i=16;i<32;i++) printf("%8x ",evbuf[i]);
printf("\nDECI : "); for(i=0;i<16;i++) printf("%8d ",evbuf[i]);
printf("\n     : "); for(i=16;i<32;i++) printf("%8d ",evbuf[i]);
printf("\n\n");
*/

/* remember that EV_HDR words in evbuf were reserved for the event header */

      sz+=EV_HDRLEN;
      if( write(f,evbuf,sz) != sz ) fini("disk write error"," ");

      bytes_sent+=sz;
      kev++;
/*      if(kev%50==0) printf("%d events %d bytes\n",kev,bytes_sent); */
    }
  }
  close(f);

  drop_connection();
  exit(0);
}


void fini(char* stop_message, char* aux_message) 
{
  printf("** rawsaver ** %s %s\n",stop_message, aux_message);
  printf("   Stop after %d events: %s\n\n",kev);
  exit(0);
}


static char* timestamp(void) {
static char buf[30];
char* p;
time_t t;
time(&t);
strcpy(buf,ctime(&t));
for(p=buf;*p;p++)
        switch(*p)
        {
         case ' ':
                *p='_';
                break;
         case '\n':
                *p=0;
                break;
        }
return buf;
}
